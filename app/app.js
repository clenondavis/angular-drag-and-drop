(function(){
	'use strict';

	function fooCtrl($scope, $q){
		var vm = this;

		vm.widgetsToDrag =  [
		  {
		    "id": 7,
		    "name": "Opportunity Summary",
		    "code": "7d5cee4a-9962-4f7b-8bd0-45e3ae917aa6",
		    "description": "Opportunities Summary",
		    "thumbnail": "",
		    "width": 3,
		    "maxWidth": 12,
		    "minWidth": 9,
		    "listingOrder": 1
		  },
		  {
		    "id": 8,
		    "name": "Won Opportunities",
		    "code": "34d1c231-8a41-4638-9ccc-a1e60626f21f",
		    "description": "Most Recently Won Opportunities",
		    "thumbnail": "",
		    "width": 3,
		    "maxWidth": 6,
		    "minWidth": 6,
		    "listingOrder": 2
		  },
		  {
		    "id": 9,
		    "name": "Lost Opportunities",
		    "code": "1e9a1039-7341-494e-9716-479c2e7c0982",
		    "description": "Most Recently Lost Opportunities",
		    "thumbnail": "",
		    "width": 3,
		    "maxWidth": 6,
		    "minWidth": 6,
		    "listingOrder": 3
		  },
		  {
		    "id": 10,
		    "name": "InProgress Opportunities",
		    "code": "5a25c37c-61b2-4bd0-a1b2-c159b186aae0",
		    "description": "Most Recently InProgress Opportunities",
		    "thumbnail": "",
		    "width": 3,
		    "maxWidth": 6,
		    "minWidth": 6,
		    "listingOrder": 4
		  }
		];

		vm.widgetsDrop = [
		  {
		    "id": 7,		    
		    "widgetId": 7,
		    "width": 3,
		    "order": 1,
		    "code": "7d5cee4a-9962-4f7b-8bd0-45e3ae917aa6"
		  },
		  {
		    "id": 8,		    
		    "widgetId": 8,
		    "width": 3,
		    "order": 2,
		    "code": "34d1c231-8a41-4638-9ccc-a1e60626f21f"
		  }
		];

		vm.dragOptions = {			
			animate: true,
			placeholder: true
		};

		vm.dragYouiOptions = {			
			opacity: 0.3,			
			revert: 'invalid'
		};

		vm.droppOptions = {
			onDrop: 'vm.onDrop',
			//beforeDrop: 'vm.beforeDrop'
		};

		vm.onDrop = onDrop;
		vm.beforeDrop  = beforeDrop ;

		vm.getWidgetNameByCode = getWidgetNameByCode;	

		function increase(item) {
			const count = 3;
			var newWidth = item.defaultWidth - count
			
			if(newWidth >= 3 ) {
				item.defaultWidth = newWidth;
				vm.cancelIncrease[item.id] = false;
			} else {
				vm.cancelIncrease[item.id] = true;
			}
		}

		function getWidgetNameByCode(widget) {
			for (var i = 0; i < vm.widgetsToDrag.length; i++) {
				var item = vm.widgetsToDrag[i];

				if(item.code === widget.code) {
					var itemName = item.name;
				}
			}

			return itemName;
		}

		function onDrop(event, ui) {
			var itemDropped = angular.element(ui.draggable).scope().item;			
			console.log('you dropped something', itemDropped);			
		}

		function beforeDrop(event, ui) {
			console.log('test');
			var itemDropped = angular.element(ui.draggable).scope().item;			
			var deferred = $q.defer();

			// return {						    
			//     "widgetId": itemDropped.id,
			//     "width": itemDropped.defaultWidth,			    
			//     "widgetCode": itemDropped.code
			// };
		}

		$scope.list1 = [
			{ 'title': 'L', 'drag': true },
			{ 'title': 'O', 'drag': true },
			{ 'title': 'M', 'drag': true },
			{ 'title': 'L', 'drag': true },
			{ 'title': 'G', 'drag': true },
			{ 'title': 'U', 'drag': true }
		];
		$scope.list2 = [
			{ 'title': 'L', 'drag': true },			
		];
	}

	/**
	* @ngdoc object
	* @name module.app
	* @description Main module
	*/
//ngDragDrop
//dndLists
	angular
		.module('app', ['ngDragDrop'])
		/**
		 * @ngdoc controller
		 * @name app.controller:fooCtrl
		 * @description mian module controller
		 */
		.controller('fooCtrl', fooCtrl);
})();